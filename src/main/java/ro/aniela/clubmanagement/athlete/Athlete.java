package ro.aniela.clubmanagement.athlete;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ro.aniela.clubmanagement.category.Category;
import ro.aniela.clubmanagement.club.Club;
import ro.aniela.clubmanagement.result.Result;

import javax.print.DocFlavor;
import java.time.LocalDate;
import java.util.List;

@Entity(name = "Athlete")
@Table(name = "athlete")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Athlete {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "surname", nullable = false)
    private String surname;
    @JoinColumn(name = "club_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Club club;
    @JoinColumn(name = "category_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;
    @Column(name = "dob")
    private LocalDate dob;
    @Column(name = "gender", nullable = false)
    private String gender;
    @Column(name = "address", nullable = false)
    private String address;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "athlete")
    private List<Result> results;
}
