package ro.aniela.clubmanagement.athlete;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/athletes")
@RequiredArgsConstructor
public class AthleteController {
    private final AthletesService athletesService;

    @GetMapping("/{id}")
    public ResponseEntity<AthleteDto> getAthlete(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(athletesService.getAthlete(id));
    }

    @GetMapping
    public ResponseEntity<List<AthleteDto>> getAll() {
        return ResponseEntity.ok(athletesService.getAllAthlete());
    }

    @PostMapping
    public ResponseEntity<AthleteDto> saveAthlete(@RequestBody AthleteDto athleteDto) {
        return new ResponseEntity(athletesService.saveAthlete(athleteDto), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<AthleteDto> updateAthlete(@PathVariable("id") Integer id, @RequestBody AthleteDto athleteDto) {
        return ResponseEntity.ok(athletesService.updateAthlete(id, athleteDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAthleteById(@PathVariable("id") Integer id) {
        athletesService.deleteAthlete(id);
        return ResponseEntity.noContent().build();
    }

}
