package ro.aniela.clubmanagement.athlete;

import ro.aniela.clubmanagement.category.Category;
import ro.aniela.clubmanagement.club.Club;

import java.time.LocalDate;

public record AthleteDto(Integer id, String name, String surname, Integer clubId, Integer categoryId, LocalDate dob,
                         String gender, String address) {
}
