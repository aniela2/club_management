package ro.aniela.clubmanagement.athlete;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AthleteRepository extends JpaRepository<Athlete, Integer> {
//    @Query(value = """
//            delete a
//            from Athlete a
//            where a.id = :id
//    """)
//    void deleteAthleteById(@Param("id") Integer id);
}
