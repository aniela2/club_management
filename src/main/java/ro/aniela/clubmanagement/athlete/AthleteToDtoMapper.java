package ro.aniela.clubmanagement.athlete;

import jakarta.persistence.Column;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class AthleteToDtoMapper implements Function<Athlete, AthleteDto> {
    public AthleteDto apply(Athlete athlete) {
        return new AthleteDto(athlete.getId(), athlete.getName(), athlete.getSurname(),
                athlete.getClub().getId(), athlete.getCategory().getId(), athlete.getDob(),
                athlete.getGender(), athlete.getAddress());
    }
}
