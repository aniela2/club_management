package ro.aniela.clubmanagement.athlete;

import jakarta.websocket.server.ServerEndpoint;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.w3c.dom.stylesheets.LinkStyle;
import ro.aniela.clubmanagement.category.Category;
import ro.aniela.clubmanagement.category.CategoryRepository;
import ro.aniela.clubmanagement.club.Club;
import ro.aniela.clubmanagement.club.ClubRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AthletesService {
    private final AthleteRepository athleteRepository;
    private final AthleteToDtoMapper athleteToDtoMapper;
    private final CategoryRepository categoryRepository;
    private final ClubRepository clubRepository;

    public AthleteDto getAthlete(Integer id) {
        Athlete athlete = athleteRepository.getReferenceById(id);
        return athleteToDtoMapper.apply(athlete);
    }

    public List<AthleteDto> getAllAthlete() {
        return athleteRepository.findAll().stream().map(a -> athleteToDtoMapper.apply(a)).toList();
    }

    public AthleteDto saveAthlete(AthleteDto athleteDto) {
        Athlete athlete = new Athlete();
        athlete.setName(athleteDto.name());
        athlete.setSurname(athleteDto.surname());
        athlete.setClub(clubRepository.getReferenceById(athleteDto.clubId()));
        athlete.setCategory(categoryRepository.getReferenceById(athleteDto.categoryId()));
        athlete.setDob(athleteDto.dob());
        athlete.setGender(athleteDto.gender());
        athlete.setAddress(athleteDto.address());
        athleteRepository.save(athlete);
        return athleteToDtoMapper.apply(athlete);
    }

    public AthleteDto updateAthlete(Integer id, AthleteDto athleteDto) {
        Athlete athlete = athleteRepository.getReferenceById(id);
        athlete.setName(athleteDto.name());
        athlete.setSurname(athleteDto.surname());
        athlete.setClub(clubRepository.getReferenceById(athleteDto.clubId()));
        athlete.setCategory(categoryRepository.getReferenceById(athleteDto.categoryId()));
        athlete.setDob(athleteDto.dob());
        athlete.setGender(athleteDto.gender());
        athlete.setAddress(athleteDto.address());
        athleteRepository.save(athlete);
        return athleteToDtoMapper.apply(athlete);
    }

    public void deleteAthlete(Integer id) {
//        athleteRepository.deleteAthleteById(id);
        athleteRepository.deleteById(id);
    }
}
