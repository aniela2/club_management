package ro.aniela.clubmanagement.category;

public record CategoryDto(Integer id, String name, String gender) {
}
