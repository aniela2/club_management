package ro.aniela.clubmanagement.category;

import jdk.dynalink.linker.LinkerServices;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor

public class CategoryService {
    private final CategoryRepository categoryRepository;
    private final CategoryToDtoMapper categoryToDtoMapper;

    public CategoryDto getCategoryByID(Integer id) {
        Category category = categoryRepository.getReferenceById(id);
        return categoryToDtoMapper.apply(category);
    }

    public CategoryDto saveCategory(CategoryDto categoryDto) {
        Category category = new Category();
        category.setName(categoryDto.name());
        category.setGender(categoryDto.gender());
        categoryRepository.save(category);
        return categoryToDtoMapper.apply(category);
    }

    public CategoryDto updateCategory(Integer id, CategoryDto categoryDto) {
        Category category = categoryRepository.getReferenceById(id);
        category.setName(categoryDto.name());
        category.setGender(categoryDto.gender());
        categoryRepository.save(category);
        return categoryToDtoMapper.apply(category);
    }

    public List<CategoryDto> getAllCategories() {
        return categoryRepository.findAll().stream().map(c -> categoryToDtoMapper.apply(c)).toList();
    }

    public void deleteCategory(Integer id) {
        categoryRepository.deleteById(id);
    }
}
