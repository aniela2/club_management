package ro.aniela.clubmanagement.category;

import org.springframework.stereotype.Component;

import java.util.function.Function;
@Component
public class CategoryToDtoMapper implements Function<Category, CategoryDto> {

    public CategoryDto apply(Category category) {
        return new CategoryDto(category.getId(), category.getName(), category.getGender());
    }
}
