package ro.aniela.clubmanagement.club;

import jakarta.persistence.*;
import lombok.*;
import ro.aniela.clubmanagement.athlete.Athlete;
import ro.aniela.clubmanagement.organization.Organization;

import java.util.List;

@Entity(name = "Club")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "club")
public class Club {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "country", nullable = false)
    private String country;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organization_id", nullable = false)
    private Organization organization;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "club")
    private List<Athlete> athletes;
}
