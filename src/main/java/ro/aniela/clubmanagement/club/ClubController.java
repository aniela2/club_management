package ro.aniela.clubmanagement.club;

import lombok.RequiredArgsConstructor;
import org.apache.coyote.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ro.aniela.clubmanagement.category.CategoryService;

import java.util.List;

@RestController
@RequestMapping("/v1/clubs")
@RequiredArgsConstructor
public class ClubController {

    private final ClubServices clubServices;

    @GetMapping("/{id}")
    public ResponseEntity<ClubDto> getClub(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(clubServices.getClubById(id));
    }

    @GetMapping
    public ResponseEntity<List<ClubDto>> getAllClubs() {
        return ResponseEntity.ok(clubServices.getAllClubs());
    }

    @PostMapping
    public ResponseEntity<ClubDto> saveClub(@RequestBody ClubDto clubDto) {
        return new ResponseEntity(clubServices.saveClub(clubDto), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ClubDto> updateClub(@RequestBody ClubDto clubDto, @PathVariable("id") Integer id) {
        return ResponseEntity.ok(clubServices.updateClub(clubDto, id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteClub(@PathVariable("id") Integer id) {
        clubServices.deleteClub(id);
        return ResponseEntity.noContent().build();
    }
}
