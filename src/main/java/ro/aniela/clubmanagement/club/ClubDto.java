package ro.aniela.clubmanagement.club;

public record ClubDto(Integer id, String name, String country, Integer organizationId) {

}
