package ro.aniela.clubmanagement.club;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ro.aniela.clubmanagement.category.CategoryRepository;
import ro.aniela.clubmanagement.organization.OrganizationRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ClubServices {

    private final ClubRepository clubRepository;
    private final ClubToDtoMapper clubToDtoMapper;
    private final OrganizationRepository organizationRepository;

    public ClubDto getClubById(Integer id) {
        Club club = clubRepository.getReferenceById(id);
        return clubToDtoMapper.apply(club);
    }

    public List<ClubDto> getAllClubs() {
        return clubRepository.findAll().stream().map(cat -> clubToDtoMapper.apply(cat)).toList();
    }

    public ClubDto saveClub(ClubDto clubDto) {
        Club club = new Club();
        club.setName(clubDto.name());
        club.setCountry(clubDto.country());
        club.setOrganization(organizationRepository.getReferenceById(clubDto.organizationId()));
        clubRepository.save(club);
        return clubToDtoMapper.apply(club);
    }

    public ClubDto updateClub(ClubDto clubDto, Integer id) {
        Club club = clubRepository.getReferenceById(id);
        club.setCountry(clubDto.country());
        club.setName(clubDto.name());
        club.setOrganization(organizationRepository.getReferenceById(clubDto.organizationId()));
        clubRepository.save(club);
        return clubToDtoMapper.apply(club);
    }

    public void deleteClub(Integer id) {
        clubRepository.deleteById(id);
    }
}
