package ro.aniela.clubmanagement.club;

import org.springframework.stereotype.Component;
import ro.aniela.clubmanagement.competition.CompetitionDto;

import java.util.function.Function;

@Component
public class ClubToDtoMapper implements Function<Club, ClubDto> {
    public ClubDto apply(Club club) {
        return new ClubDto(club.getId(), club.getName(), club.getCountry(), club.getOrganization().getId());
    }

}
