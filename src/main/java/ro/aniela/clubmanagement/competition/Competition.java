package ro.aniela.clubmanagement.competition;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import ro.aniela.clubmanagement.result.Result;

import java.time.LocalDate;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity(name = "Competition")
@Table(name = "competition")
public class Competition {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "town")
    private String town;
    @Column(name = "venue_date", nullable = false)
    private LocalDate venueDate;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "competition")
    private List<Result> results;
}
