package ro.aniela.clubmanagement.competition;

import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/competitions")
@RequiredArgsConstructor
public class CompetitionController {

    private final CompetitionService competitionService;

    @GetMapping("/{id}")
    public ResponseEntity<CompetitionDto> getCompetition(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(competitionService.getCompetition(id));
    }

    @PostMapping
    public ResponseEntity<CompetitionDto> saveCompetition(@RequestBody CompetitionDto competitionDto) {
        return new ResponseEntity(competitionService.saveCompetition(competitionDto), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CompetitionDto> updateCompetition(@PathVariable("id") Integer id, @RequestBody CompetitionDto competitionDto) {
        return ResponseEntity.ok(competitionService.updateCompetition(id, competitionDto));
    }

    @GetMapping
    public ResponseEntity<List<CompetitionDto>> getAllCompetitions() {
        return ResponseEntity.ok(competitionService.getAllCompetitions());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCompetition(@PathVariable("id") Integer id) {
        competitionService.deleteCompetition(id);
        return ResponseEntity.noContent().build();
    }
}
