package ro.aniela.clubmanagement.competition;

import org.springframework.cglib.core.Local;

import java.time.LocalDate;

public record CompetitionDto (Integer id, String name, String town, LocalDate venueDate){

}
