package ro.aniela.clubmanagement.competition;

import lombok.RequiredArgsConstructor;
import org.springframework.aop.target.LazyInitTargetSource;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor

public class CompetitionService {
    private final CompetitionRepository competitionRepository;
    private final CompetitionToDtoMapper competitionToDtoMapper;


    public CompetitionDto getCompetition(Integer id) {
        Competition competition = competitionRepository.getReferenceById(id);
        return competitionToDtoMapper.apply(competition);
    }

    public CompetitionDto saveCompetition(CompetitionDto competitionDto) {
        Competition competition = new Competition();
        competition.setName(competitionDto.name());
        competition.setTown(competitionDto.town());
        competition.setVenueDate(competitionDto.venueDate());
        competitionRepository.save(competition);
        return competitionToDtoMapper.apply(competition);
    }

    public CompetitionDto updateCompetition(Integer id, CompetitionDto competitionDto) {
        Competition competition = competitionRepository.getReferenceById(id);
        competition.setName(competitionDto.name());
        competition.setTown(competitionDto.town());
        competition.setVenueDate(competitionDto.venueDate());
        competitionRepository.save(competition);
        return competitionToDtoMapper.apply(competition);
    }

    public List<CompetitionDto> getAllCompetitions() {
        return competitionRepository.findAll().stream().map(c -> competitionToDtoMapper.apply(c)).toList();
    }

    public void deleteCompetition(Integer id) {
        competitionRepository.deleteById(id);
    }

}
