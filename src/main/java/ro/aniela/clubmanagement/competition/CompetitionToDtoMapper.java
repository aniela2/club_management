package ro.aniela.clubmanagement.competition;

import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class CompetitionToDtoMapper implements Function<Competition, CompetitionDto> {
    public CompetitionDto apply(Competition competition) {
        return new CompetitionDto(competition.getId(), competition.getName(), competition.getTown(), competition.getVenueDate());
    }
}
