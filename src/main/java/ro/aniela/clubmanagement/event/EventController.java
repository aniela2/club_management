package ro.aniela.clubmanagement.event;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/events")
public class EventController {
    private final EventService eventService;

    @GetMapping("/{id}")
    public ResponseEntity<EventDto> getEventByID(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(eventService.getEventById(id));
    }

    @GetMapping
    public ResponseEntity<List<EventDto>> getAllEvent() {
        return ResponseEntity.ok(eventService.getAllEvent());
    }

    @PostMapping
    public ResponseEntity<EventDto> saveEvent(@RequestBody EventDto eventDto) {
        return new ResponseEntity(eventService.saveEvent(eventDto), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<EventDto> updateEvent(@PathVariable Integer id, @RequestBody EventDto eventDto) {
        return ResponseEntity.ok(eventService.updateEvent(id, eventDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteEvent(@PathVariable("id") Integer id) {
        eventService.deleteEvent(id);
        return ResponseEntity.noContent().build();
    }
}
