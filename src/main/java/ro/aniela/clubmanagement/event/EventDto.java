package ro.aniela.clubmanagement.event;

public record EventDto(Integer id, String name, Integer organizationId) {
}
