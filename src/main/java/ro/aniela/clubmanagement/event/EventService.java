package ro.aniela.clubmanagement.event;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ro.aniela.clubmanagement.organization.OrganizationRepository;

import java.util.List;

@Service
@RequiredArgsConstructor

public class EventService {

    private final EventRepository eventRepository;
    private final EventToDtoMapper eventToDtoMapper;
    private final OrganizationRepository organizationRepository;

    public EventDto getEventById(Integer id) {
        return eventToDtoMapper.apply(eventRepository.getReferenceById(id));
    }

    public List<EventDto> getAllEvent() {
        return eventRepository.findAll().stream().map(e -> eventToDtoMapper.apply(e)).toList();
    }

    public EventDto saveEvent( EventDto eventDto) {
        Event event = new Event();
        event.setId(event.getId());
        event.setName(eventDto.name());
        event.setOrganization(organizationRepository.getReferenceById(eventDto.organizationId()));
        eventRepository.save(event);
        return eventToDtoMapper.apply(event);
    }

    public EventDto updateEvent(Integer id, EventDto eventDto) {
        Event event = eventRepository.getReferenceById(id);
        event.setName(eventDto.name());
        event.setOrganization(organizationRepository.getReferenceById(eventDto.organizationId()));
        eventRepository.save(event);
        return eventToDtoMapper.apply(event);
    }

    public void deleteEvent(Integer id) {
        eventRepository.deleteById(id);
    }
}
