package ro.aniela.clubmanagement.event;

import org.springframework.stereotype.Component;

import java.util.function.Function;
@Component
public class EventToDtoMapper implements Function<Event, EventDto> {
    public EventDto apply(Event event) {
        return new EventDto(event.getId(), event.getName(), event.getOrganization().getId());
    }

}
