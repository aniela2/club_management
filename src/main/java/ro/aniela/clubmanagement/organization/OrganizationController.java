package ro.aniela.clubmanagement.organization;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1/organizations")
@RequiredArgsConstructor
public class OrganizationController {
    private final OrganizationService organizationService;

    @GetMapping("/{id}")
    private ResponseEntity<OrganizationDto> getOrganizationById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(organizationService.getOrganizationById(id));
    }

    @GetMapping
    private ResponseEntity<List<OrganizationDto>> getAllOrganizations() {
        return ResponseEntity.ok(organizationService.getAllOrganizations());
    }

    @PostMapping
    private ResponseEntity<OrganizationDto> saveOrganization(@RequestBody OrganizationDto organizationDto) {
        return new ResponseEntity(organizationService.saveOrganization(organizationDto), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    private ResponseEntity<OrganizationDto> updateOrganization(@RequestBody OrganizationDto organizationDto, @PathVariable("id") Integer id) {
        return ResponseEntity.ok(organizationService.updateOrganization(organizationDto, id));
    }

    @DeleteMapping("/{id}")
    private ResponseEntity<Void> deleteOrganization(@PathVariable("id") Integer id){
       organizationService.deleteOrganization(id);
        return ResponseEntity.noContent().build();
    }
}
