package ro.aniela.clubmanagement.organization;

public record OrganizationDto(Integer id, String name) {
}
