package ro.aniela.clubmanagement.organization;

import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class OrganizationMapper implements Function<Organization, OrganizationDto> {
    @Override
    public OrganizationDto apply(Organization organization) {
        return new OrganizationDto(organization.getId(), organization.getName());
    }
}
