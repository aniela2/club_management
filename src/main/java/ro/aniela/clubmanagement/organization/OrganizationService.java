package ro.aniela.clubmanagement.organization;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.List;


@Service
@RequiredArgsConstructor
public class OrganizationService {
    private final OrganizationRepository organizationRepository;
    private final OrganizationMapper organizationMapper;

    public OrganizationDto getOrganizationById(Integer id) {
        Organization organization = organizationRepository.getReferenceById(id);
        return organizationMapper.apply(organization);
    }

    public List<OrganizationDto> getAllOrganizations() {
        return organizationRepository.findAll().stream().map(o -> organizationMapper.apply(o)).toList();
    }

    public OrganizationDto saveOrganization(OrganizationDto organizationDto) {
        Organization o = new Organization();
        o.setName(organizationDto.name());
        return organizationMapper.apply(organizationRepository.save(o));
    }


    public OrganizationDto updateOrganization(OrganizationDto organizationDto, Integer id) {
        Organization o = organizationRepository.getReferenceById(id);
        o.setName(organizationDto.name());
        return organizationMapper.apply(organizationRepository.save(o));
    }

    public void deleteOrganization(Integer id){
      organizationRepository.deleteById(id);
    }
}
