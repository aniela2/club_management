package ro.aniela.clubmanagement.result;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ro.aniela.clubmanagement.athlete.Athlete;
import ro.aniela.clubmanagement.club.Club;
import ro.aniela.clubmanagement.competition.Competition;
import ro.aniela.clubmanagement.event.Event;

@Table(name = "result")
@Data
@Entity(name = "Result")
@AllArgsConstructor
@NoArgsConstructor
public class Result {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @JoinColumn(name = "athlete_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Athlete athlete;
    @Column(name = "value", nullable = false)
    private String value;
    @JoinColumn(name = "competition_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Competition competition;
    @JoinColumn(name = "event_id", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Event event;
}
