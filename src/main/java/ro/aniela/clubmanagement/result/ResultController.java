package ro.aniela.clubmanagement.result;

import jakarta.persistence.Column;
import jakarta.persistence.criteria.CriteriaBuilder;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("v1/results")
@RequiredArgsConstructor

public class ResultController {
    private final ResultService resultService;

    @GetMapping("/{id}")
    public ResponseEntity<ResultDto> getResultById(@PathVariable("id") Integer id) {
        return ResponseEntity.ok(resultService.getResultById(id));
    }

    @GetMapping
    public ResponseEntity<List<ResultDto>> getAllResult() {
        return ResponseEntity.ok(resultService.getAll());
    }

    @PostMapping
    public ResponseEntity<ResultDto> saveResult(@RequestBody ResultDto resultDto) {

        return new ResponseEntity(resultService.saveResult(resultDto), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResultDto> updateResult(@PathVariable("id") Integer id, @RequestBody ResultDto resultDto) {
        return ResponseEntity.ok(resultService.updateResult(id, resultDto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteResult(@PathVariable("id") Integer id) {
        resultService.deleteResult(id);
        return ResponseEntity.noContent().build();
    }
}
