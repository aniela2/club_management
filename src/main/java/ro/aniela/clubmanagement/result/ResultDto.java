package ro.aniela.clubmanagement.result;

import ro.aniela.clubmanagement.athlete.Athlete;

public record ResultDto(Integer id, Integer athleteId, String value, Integer competitionId, Integer eventId){

}
