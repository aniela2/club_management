package ro.aniela.clubmanagement.result;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ro.aniela.clubmanagement.athlete.Athlete;
import ro.aniela.clubmanagement.athlete.AthleteRepository;
import ro.aniela.clubmanagement.competition.CompetitionRepository;
import ro.aniela.clubmanagement.event.EventRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ResultService {
    private final ResultRepository resultRepository;
    private final ResultToDtoMapper resultToDtoMapper;
    private final AthleteRepository athleteRepository;
    private final CompetitionRepository competitionRepository;
    private final EventRepository eventRepository;

    public ResultDto getResultById(Integer id) {
        Result result = resultRepository.getReferenceById(id);
        return resultToDtoMapper.apply(result);
    }

    public List<ResultDto> getAll() {
        return resultRepository.findAll().stream().map(c -> resultToDtoMapper.apply(c)).toList();
    }

    public ResultDto saveResult(ResultDto resultDto) {
        Result result = new Result();
        result.setAthlete(athleteRepository.getReferenceById(resultDto.athleteId()));
        result.setValue(resultDto.value());
        result.setCompetition(competitionRepository.getReferenceById(resultDto.competitionId()));
        result.setEvent(eventRepository.getReferenceById(resultDto.eventId()));
        resultRepository.save(result);
        return resultToDtoMapper.apply(result);
    }

    public ResultDto updateResult(Integer id, ResultDto resultDto) {
        Result result = resultRepository.getReferenceById(id);
        result.setAthlete(athleteRepository.getReferenceById(resultDto.athleteId()));
        result.setValue(resultDto.value());
        result.setCompetition(competitionRepository.getReferenceById(resultDto.competitionId()));
        result.setEvent(eventRepository.getReferenceById(resultDto.eventId()));
        resultRepository.save(result);
        return resultToDtoMapper.apply(result);
    }

    public void deleteResult(Integer id) {
        resultRepository.deleteById(id);
    }
}
