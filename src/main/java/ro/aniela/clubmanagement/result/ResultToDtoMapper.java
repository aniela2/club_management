package ro.aniela.clubmanagement.result;

import jakarta.persistence.Column;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class ResultToDtoMapper implements Function<Result, ResultDto> {
    public ResultDto apply(Result result) {
        return new ResultDto(result.getId(),
                result.getAthlete().getId(), result.getValue(),
                result.getCompetition().getId(), result.getEvent().getId());
    }
}
