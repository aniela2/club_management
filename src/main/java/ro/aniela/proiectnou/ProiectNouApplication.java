package ro.aniela.proiectnou;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ProiectNouApplication {
    public static void main(String[] args) {
        SpringApplication.run(ProiectNouApplication.class, args);
    }
}
