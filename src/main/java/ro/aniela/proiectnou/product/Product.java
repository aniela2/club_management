package ro.aniela.proiectnou.product;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ro.aniela.proiectnou.productcategory.ProductCategory;

@Entity(name = "Product")
@Table(name = "product")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "price")
    private Double price;
    @Column(name = "weight")
    private Double weight;
    @JoinColumn(name = "product_category_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private ProductCategory productCategory;

}
