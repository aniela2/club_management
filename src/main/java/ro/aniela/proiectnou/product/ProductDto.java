package ro.aniela.proiectnou.product;

import ro.aniela.proiectnou.productcategory.ProductCategory;

public record ProductDto(Integer id, String name, Double price, Double weight, Integer productCategoryId) {

}
