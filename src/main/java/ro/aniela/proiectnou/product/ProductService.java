package ro.aniela.proiectnou.product;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ro.aniela.proiectnou.productcategory.ProductCategory;
import ro.aniela.proiectnou.productcategory.ProductCategoryRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {
    private ProductRepository productRepository;
    private ProductToDtoMapper productToDtoMapper;
    private ProductCategoryRepository productCategoryRepository;

    public ProductDto getProductById(Integer id) {
        Product know = productRepository.getReferenceById(id);
        return productToDtoMapper.apply(know);
    }

    public List<ProductDto> getAllProducts() {
        return productRepository.findAll().stream().map(p -> productToDtoMapper.apply(p)).toList();
    }

    public ProductDto saveProduct(ProductDto productDto) {
        Product product = new Product();
        product.setName(productDto.name());
        product.setPrice(productDto.price());
        product.setWeight(productDto.weight());
        product.setProductCategory(productCategoryRepository.getReferenceById(productDto.productCategoryId()));
        productRepository.save(product);
        return productToDtoMapper.apply(product);
    }

    public ProductDto updateProduct(Integer id, ProductDto productDto) {
        Product product = productRepository.getReferenceById(id);
        product.setName(productDto.name());
        product.setPrice(productDto.price());
        product.setWeight(productDto.weight());
        product.setProductCategory(productCategoryRepository.getReferenceById(productDto.productCategoryId()));
        productRepository.save(product);
        return productToDtoMapper.apply(product);
    }

    public void deleteProduct(Integer id) {
        productRepository.deleteById(id);
    }
}
