package ro.aniela.proiectnou.product;

import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class ProductToDtoMapper implements Function<Product, ProductDto> {
    public ProductDto apply(Product product) {
        return new ProductDto(product.getId(), product.getName(), product.getPrice(), product.getWeight(), product.getProductCategory().getId());
    }
}
