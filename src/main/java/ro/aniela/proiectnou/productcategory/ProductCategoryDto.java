package ro.aniela.proiectnou.productcategory;

public record ProductCategoryDto(Integer id, String type, String name) {
}
