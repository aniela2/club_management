package ro.aniela.proiectnou.productcategory;

import org.springframework.stereotype.Component;

import java.util.function.Function;
@Component
public class ProductCategoryToDtoMapper implements Function<ProductCategory, ProductCategoryDto> {
    public ProductCategoryDto apply(ProductCategory productCategory) {
        return new ProductCategoryDto(productCategory.getId(), productCategory.getType(), productCategory.getName());
    }
}
